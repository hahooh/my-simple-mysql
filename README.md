# My Simple DB
Just want to create a free simple GUI MySQL / MariaDB client.

## For who?
* I built this for myself, if you like it that is good.
* It is definitely not for advanced user, for example database administrators.

## Parts
* ElectronJS (which means memory intensive)
* Svelte (my first time use, why not)
* MySQL2

## Supporting DB Managers
* For now It only supports 2 DB (basiacally the same), MySQL and Mariadb.

## Near Future updates
* Maybe safe guards around custom query which mean be careful when you are using it now

## Far Future updates
* Maybe more DBMS supports? (not sure)

## How to start
Clone this repo.
```
npm i && npm start
```
Will start the app with watcher and dev tool open.
## How to build 
Check `package.json` before build this project and check you build OS.
```
npm run make
```
will create executable files in `out`
## If you like my project
<a href="https://www.buymeacoffee.com/hahooh" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-orange.png" alt="Buy Me A Hot Chocolate" height="41" width="174"></a>

## MIT License
Copyright (c) 2021 hahooh

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
