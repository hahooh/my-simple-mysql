export function showRowValue(value) {
    if (value == null) {
        return "NULL";
    }
    if (typeof value.getMonth === "function" && value instanceof Date) {
        return formatDate(value);
    }
    return value;
}

export function formatDate(date) {
    function leadingZero(number) {
        if (number < 10) {
            return `0${number}`;
        }
        return `${number}`;
    }

    const year = date.getFullYear();
    const month = leadingZero(date.getMonth() + 1);
    const d = leadingZero(date.getDate());
    const h = leadingZero(date.getHours());
    const m = leadingZero(date.getMinutes());
    const s = leadingZero(date.getSeconds());
    return `${year}-${month}-${d} ${h}:${m}:${s}`;
}