const mysql = require('mysql2/promise');
const bluebird = require('bluebird');

/**
 * adapter interface
 * connect - connection database
 * disconnect - disconnect database
 * userDatabse - change database (use database)
 * getDatabases - list all databases
 * getTables - list all table names
 * getTableRows - list rows of tables
 * customQuery - run custom query
 * showCreateTable - show create table
 * get hasConnection boolean
 * get hasConnectionInfo boolean
 * get connectionInfo objext
 * set connectionInfo(connectionInfo) object
 */

/**
 * Create MySQLConnection
 * @class
 * @property {Function} connect connect db with connection info
 */
export default class MySQLConnection {
    get hasConnection() {
        return !!this._connection;
    }

    get hasConnectionInfo() {
        return !!this._connectionInfo;
    }

    get connectionInfo() {
        return this._connectionInfo;
    }

    /**
     * connectionInfo - { host, user, password, port, database, table }
     */
    set connectionInfo(connectionInfo) {
        this._connectionInfo = connectionInfo;
    }

    _checkConnection() {
        if (!this._connection) {
            throw new Error("Connection not exists");
        }
    }

    _createConnection() {
        return mysql.createConnection({
            host: this._connectionInfo.host || 'localhost',
            user: this._connectionInfo.user,
            password: this._connectionInfo.password,
            database: this._connectionInfo.database,
            port: this._connectionInfo.port || 3306,
            Promise: bluebird,
        });
    }

    async connect() {
        if (!this._connectionInfo) {
            throw new Error('Connection Info not exists');
        }

        this._connection = await this._createConnection();
    }

    async disconnect() {
        this._checkConnection();
        await this._connection.end();
        this._connection = null;
    }

    async useDatabase(database) {
        this._checkConnection();
        await this._connection.query(`USE ${database}`);
    }

    async getDatabases() {
        this._checkConnection();
        const [databasesRows, _] = await this._connection.query("SHOW databases");
        return databasesRows.map(row => row.Database);
    }

    async getAllTables() {
        this._checkConnection();
        const [tables, fields] = await this._connection.query('SHOW tables');
        const tableField = fields[0];
        const fieldName = tableField.name;
        return tables.map(table => table[fieldName]);
    }

    async getTableRows(tableName, limit = 100, offset = 0, order) {
        this._checkConnection();
        let values = [];
        let sql = `SELECT * FROM ${tableName} `;

        if (order && order.by && order.direction) {
            sql += ` ORDER BY ${order.by} ${order.direction}`;
        }

        sql += " LIMIT ? OFFSET ?";
        values = [...values, limit, offset];
        const [rows, rowFields] = await this._connection.query({ sql, values });
        const [count, _] = await this._connection.query(`SELECT count(*) as total_rows FROM ${tableName}`);
        return {
            rows,
            count: count[0].total_rows,
            columns: rowFields.map(column => column.name),
        }
    }

    async customQuery(query) {
        this._checkConnection();
        const connection = await this._createConnection();
        const [rows, fields] = await connection.query(query);
        await connection.end();
        return {
            rows: rows || [],
            columns: fields.map(column => column.name),
        };
    }

    async showCreateTable() {
        this._checkConnection();
        const result = await this._connection.query(`SHOW CREATE TABLE ${this._connectionInfo.table}`);
        return result[0][0]['Create Table'];
    }

    async searchTablesWithColumnName(columnName) {
        this._checkConnection();
        const results = await this._connection.query({
            sql: 'SELECT DISTINCT TABLE_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE column_name = ? AND TABLE_SCHEMA = ?',
            values: [columnName, this._connectionInfo.database],
        });
        return results[0].map(result => result.TABLE_NAME);
    }
}