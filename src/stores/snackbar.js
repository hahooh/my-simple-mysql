import { writable } from "svelte/store";

function createSnackbar() {
    const { subscribe, set } = writable({ show: false, color: 'success' });
    return {
        subscribe,
        show: (message, color) => {
            set({ show: true, color, message })
        },
        hide: () => {
            set({ show: false })
        }
    }
}

export const snackbar = createSnackbar();