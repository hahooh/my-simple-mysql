import { writable } from 'svelte/store'

function createCurrentConnectionIndex() {
    const { subscribe, set } = writable(0);
    return {
        subscribe,
        setCurrentConnectionIndex: (currentConnIndex) => set(currentConnIndex),

    };
}

export const currentConnectionIndex = createCurrentConnectionIndex();