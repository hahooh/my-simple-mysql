import { writable } from "svelte/store";

function createTableSearchTerm() {
    const { subscribe, set } = writable('');
    return {
        subscribe,
        setSearchTerm: (searchTerm) => set(searchTerm),
    };
}

export const tableSearchTerm = createTableSearchTerm();

function createTableSearchBy() {
    const { subscribe, set } = writable('byName');
    return {
        subscribe,
        setSearchBy: (searchBy) => set(searchBy),
    };
}

export const tableSearchBy = createTableSearchBy();