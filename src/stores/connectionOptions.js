import { writable } from "svelte/store";

const storage = window.localStorage;
const connectionOptionsKey = "CONNECTION_OPTION_KEY";

function getConnectionOptions() {
    const connectionOptions = storage.getItem(connectionOptionsKey);
    if (!connectionOptions) {
        return [];
    }
    return JSON.parse(connectionOptions);
}

function saveOption(connectionOption) {
    setConnectionOptions([
        ...getConnectionOptions(),
        { ...connectionOption, id: new Date().getTime() },
    ]);
}

function deleteSavedConnectionOption(optionId) {
    setConnectionOptions(getConnectionOptions().filter((option) => option.id !== optionId));
}

function getLastUsedOption() {
    return getConnectionOptions().find(option => option.lastUsedOption)
}

function setLastUsed(optionId) {
    setConnectionOptions(getConnectionOptions().map(option => {
        if (option.id === optionId) {
            option.lastUsedOption = true;
        } else {
            option.lastUsedOption = false;
        }
        return option;
    }))
}

function setConnectionOptions(connectionOptions) {
    storage.setItem(
        connectionOptionsKey,
        JSON.stringify(connectionOptions)
    );
};

function createConnectionOptions() {
    const { subscribe, set } = writable(getConnectionOptions());
    return {
        subscribe,
        saveOption: (option) => {
            saveOption(option);
            set(getConnectionOptions());
        },
        deleteSavedConnectionOption: (optionId) => {
            deleteSavedConnectionOption(optionId);
            set(getConnectionOptions());
        },
        getLastUsedOption: () => {
            return getLastUsedOption();
        },
        setLastUsed: (optionId) => {
            setLastUsed(optionId);
            set(getConnectionOptions());
        },
    };
}

export const connectionOptions = createConnectionOptions();