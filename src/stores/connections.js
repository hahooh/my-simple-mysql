import { writable, get, derived } from 'svelte/store'
import MySQLConnection from '../scripts/adapters/mysql';
import { currentConnectionIndex } from './currentConnectionIndex';

function createConnections() {
    const { subscribe, set } = writable([new MySQLConnection()]);
    return {
        subscribe,
        getCurrentConnection: () => {
            let connections = [];
            subscribe(conns => {
                connections = conns;
            });
            return connections[get(currentConnectionIndex)];
        },
        useDatabase: async (database) => {
            let connections = [];
            subscribe(conns => {
                connections = conns;
            });

            /**
             * @type {MySQLConnection}
             */
            const targetConn = connections[get(currentConnectionIndex)];
            targetConn.connectionInfo = {
                ...targetConn.connectionInfo,
                database,
            };

            await targetConn.useDatabase(database);
            set(connections);
        },
        selectTable: async (table) => {
            let connections = [];
            subscribe(conns => {
                connections = conns;
            });

            /**
             * @type {MySQLConnection}
             */
            const targetConn = connections[get(currentConnectionIndex)];
            targetConn.connectionInfo = {
                ...targetConn.connectionInfo,
                table,
            };

            set(connections);
        },
        addInitConnection: async () => {
            let connections = [];
            subscribe(conns => {
                connections = conns;
            });

            /**
             * @type {MySQLConnection}
             */
            const currentConn = connections[get(currentConnectionIndex)];
            if (currentConn && currentConn.hasConnection) {
                await currentConn.disconnect();
            }

            /**
             * @type {String | null | undefined}
             */
            if (currentConn && (currentConn.hasConnectionInfo || {}).table) {
                currentConn.connectionInfo = {
                    ...currentConn.connectionInfo,
                    table: currentConn.hasConnectionInfo.table,
                };
            }

            if (connections.every(conn => conn.hasConnectionInfo)) {
                connections = [...connections, new MySQLConnection()];
            }
            set(connections);
            currentConnectionIndex.setCurrentConnectionIndex(connections.length - 1);
        },
        initConnection: async (connectionInfo) => {
            let connections = [];
            subscribe(conns => {
                connections = conns;
            });

            /**
             * @type {MySQLConnection}
             */
            const conn = connections[get(currentConnectionIndex)];
            conn.connectionInfo = connectionInfo;
            await conn.connect();
            set(connections);
        },
        changeConnection: async (nextConnectionIndex) => {
            const currConnIndex = get(currentConnectionIndex);
            if (currConnIndex === nextConnectionIndex) {
                return;
            }

            let connections = [];
            subscribe(conns => {
                connections = conns;
            });

            if (nextConnectionIndex > (connections || []).length - 1) {
                return;
            }

            /**
             * @type {MySQLConnection}
             */
            const currentConn = connections[currConnIndex];
            if (currentConn && currentConn.hasConnection) {
                await currentConn.disconnect();
            }

            /**
             * @type {String | null | undefined}
             */
            if (currentConn && (currentConn.hasConnectionInfo || {}).table) {
                currentConn.connectionInfo = {
                    ...currentConn.connectionInfo,
                    table: currentConn.hasConnectionInfo.table,
                };
            }

            /**
             * @type {MySQLConnection}
             */
            const nextConn = connections[nextConnectionIndex];
            if (nextConn && nextConn.hasConnectionInfo) {
                await nextConn.connect();
            }

            set(connections);
            currentConnectionIndex.setCurrentConnectionIndex(nextConnectionIndex);
        },
        removeConnection: async (connectionIndex) => {
            let connections = [];
            subscribe(conns => {
                connections = conns;
            });

            /**
             * @type {MySQLConnection}
             */
            const targetConn = connections[connectionIndex];
            targetConn.hasConnection && await targetConn.disconnect();

            connections = (connections || []).filter((_, i) => i !== connectionIndex);
            if (connections.length === 0) {
                connections.push(new MySQLConnection());
            }

            let newCurrIndex = 0;
            let currIndex = get(currentConnectionIndex);

            if (connections.length <= currIndex) {
                newCurrIndex = connections.length - 1;
            }

            if (connectionIndex === currIndex) {
                const nextConn = connections[newCurrIndex];
                if (nextConn.hasConnectionInfo) {
                    await nextConn.connect();
                }
            }

            set(connections);
            if (newCurrIndex !== null) {
                currentConnectionIndex.setCurrentConnectionIndex(newCurrIndex);
            }
        },
        async reconnect() {
            let connections = [];
            subscribe(conns => {
                connections = conns;
            });

            const targetConn = connections[get(currentConnectionIndex)];
            try {
                targetConn.hasConnection && await targetConn.disconnect();
            } catch (e) {
                console.error(e);
            }

            await targetConn.connect();
            set(connections);
        }
    };
}

export const connections = createConnections();

export const currentConnection = derived(
    [currentConnectionIndex, connections],
    ([$currentConnectionIndex, $connections]) => $connections[$currentConnectionIndex]);